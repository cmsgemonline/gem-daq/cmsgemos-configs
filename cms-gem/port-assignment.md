# Port assignment

In order to avoid conflicts, the following table summarize the ports assignment used in the configurations.

| Configuration                        | Port range  | Comment                                         |
|--------------------------------------|-------------|-------------------------------------------------|
| `Global`                             | 20100-20199 | Shared ports for all configurations             |
| `Global/GEM-Global`                  | 20100-20109 |                                                 |
| `MiniDAQ3/GEM`                       | 20400-20499 | Shared ports for all configurations             |
| `MiniDAQ3/GEM/GEM-MiniDAQ`           | 20400-20409 |                                                 |
| `MiniDAQ3/GEM/GEM-MiniDAQ-with-CSC`  | 20400-20409 |                                                 |
| `Local`                              | 20200-20299 |                                                 |
| `Local/GEM-Local`                    | 20200-20209 |                                                 |
| `Local/GEM-Local-noTCDS`             | 20210-20219 | All AMC13 must be put in loopback mode          |
| `Local/GEMPILOT-Local-noTCDS`        | 20220-20229 | The GEMPILOT AMC13 must be put in loopback mode |
| `Monitoring`                         | 20300-20399 |                                                 |
| `Monitoring/GEM-Monitoring`          | 20300-20309 |                                                 |
| `Special`                            | 20400-20499 |                                                 |
| `Special/GEMM-TrolleyTest-noTCDS`    | 20400-20409 | The GEM- AMC13 must be put in loopback mode     |

Each group/folder is given 100 ports by default while each configuration is given 10 ports by default.
Depending on the use case, the assigned range can vary.
