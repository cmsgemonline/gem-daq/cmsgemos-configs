#!/bin/zsh

if ! [ -z ${1+x} ]
then
    crate=$1
    echo CRATE=$crate
else
    echo Crate not specified, exiting
    exit
fi

if ! [ -z ${2+x} ]
then
    cmd=$2
    echo CMD=$cmd
else
    echo Command not specified, setting to "config"
    cmd="config"
fi

if ! [ $crate = "3" ] && ! [ $crate = "4" ]
then
    echo Invalid crate specified, exiting
    exit
fi

# http://tcds-control-csc-pri.cms:2104/urn:xdaq-application:lid=30${crate}
# gempilot1:x0${crate}
# gempilot2:x04
# http://tcds-control-904.cms904:2107/urn:xdaq-application:lid=303

CI_HW_CONFIG_STRING=$( cat ${HOME}/config/tcds/hw_cfg_gem_ici.txt )
PI_HW_CONFIG_STRING=$( cat ${HOME}/config/tcds/hw_cfg_gem_pi.txt  )

# Halt CI and PI for GEM
if [ $? = "0" ]
then
    ${0:h}/sendSOAP -v -n localhost -p 12107 -A tcds::ici::ICIController -L 30${crate} -b \
          "<xdaq:Halt xdaq:actionRequestorId='ICI-GEM' xmlns:xdaq='urn:xdaq-soap:3.0'></xdaq:Halt>"
    haltici=$?
    echo $haltici
    ${0:h}/sendSOAP -v -n localhost -p 12107 -A tcds::pi::PIController -L 50${crate} -b \
          "<xdaq:Halt xdaq:actionRequestorId='PI-GEM' xmlns:xdaq='urn:xdaq-soap:3.0'></xdaq:Halt>"
    haltpi=$?
    echo $haltpi
    sleep 2
fi

# Configure CI for GEM
if [ "${haltici}" = "0" ] && [ "${haltpi}" = "0" ]
then
    ${0:h}/sendSOAP -v -n localhost -p 12107 -A tcds::ici::ICIController -L 30${crate} -b \
          "<xdaq:Configure xdaq:actionRequestorId='ICI-GEM' xmlns:xdaq='urn:xdaq-soap:3.0'><xdaq:hardwareConfigurationString xsi:type='xsd:string'>${CI_HW_CONFIG_STRING}</xdaq:hardwareConfigurationString></xdaq:Configure>"
    confici=$?
    sleep 5
    echo confici=${confici}

    # Configure PI for GEM
    if [ "${confici}" = "0" ]
    then
        ${0:h}/sendSOAP -v -n localhost -p 12107 -A tcds::pi::PIController -L 50${crate} -b \
              "<xdaq:Configure xdaq:actionRequestorId='PI-GEM' xmlns:xdaq='urn:xdaq-soap:3.0'><xdaq:hardwareConfigurationString xsi:type='xsd:string'>${PI_HW_CONFIG_STRING}</xdaq:hardwareConfigurationString><xdaq:fedEnableMask xsi:type='xsd:string'>%</xdaq:fedEnableMask></xdaq:Configure>"
        confpi=$?
        sleep 10
        echo confpi=$confpi
    else
        echo "Unable to configure ICIController 30${crate}, not trying to configure PIController 50${crate}"
        echo test1 [ ! "${confici}" = "0" ]
    fi
else
    echo "Unable to Halt ICIController 30${crate} or PIController 50${crate}, not configuring"
    echo test1 [ ! "${haltici}" = "0" ]
    echo test2 [ ! "${haltpi}" = "0" ]
fi

# Enable CI and PI for GEM
if [[ "${cmd}" =~ '(ENABLE|enable|START|start)' ]]
then
    ${0:h}/sendSOAP -v -n localhost -p 12107 -A tcds::ici::ICIController -L 30${crate} -b \
          "<xdaq:Enable xdaq:actionRequestorId='ICI-GEM' xmlns:xdaq='urn:xdaq-soap:3.0'><xdaq:runNumber xsi:type='xsd:unsignedInt'>0</xdaq:runNumber></xdaq:Enable>"
    enableici=$?
    echo $enableici
    ${0:h}/sendSOAP -v -n localhost -p 12107 -A tcds::pi::PIController -L 50${crate} -b \
          "<xdaq:Enable xdaq:actionRequestorId='PI-GEM' xmlns:xdaq='urn:xdaq-soap:3.0'><xdaq:runNumber xsi:type='xsd:unsignedInt'>0</xdaq:runNumber></xdaq:Enable>"
    enablepi=$?
    echo $enablepi
fi
